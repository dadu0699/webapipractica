var express = require('express');
var usuarioModel = require('../model/usuario.model');
var usuarioRoute = express.Router();

usuarioRoute.route('/usuario/')
  .get(function(req, res) {
    usuarioModel.selectAll(function(resultados) {
      res.json(resultados);
    });
  })
  .post(function(req, res) {
    var data = {
      nick: req.body.nick,
      contrasena: req.body.contrasena
    }

    usuarioModel.insert(data, function(resultado){
      if(typeof resultado !== undefined && resultado.insertId > 0) {
        res.json(data);
      } else {
        res.json({"Mensaje":"Se registo el usuario correctamente"});
      }
    });
  });

usuarioRoute.route('/usuario/:idUsuario')
  .get(function(req, res) {
    var idUsuario = req.params.idUsuario;
    usuarioModel.find(idUsuario, function(resultados){
      if(typeof resultados !== undefined) {
        res.json(resultados);
      } else {
        res.json({"Mensaje": "No se encontro el usuario"})
      }
    });
  })
  .put(function(req, res) {
    var idUsuario = req.params.idUsuario;

    var data = {
      idUsuario: req.body.idUsuario,
      nick: req.body.nick,
      contrasena: req.body.contrasena,
      picture: req.body.picture
    }

    if(idUsuario == data.idUsuario) {
      usuarioModel.update(data, function(resultado){
        if(typeof resultado !== undefined) {
          res.json({"Editado":"true"});
    		} else {
    			res.json({"mensaje":"No se pudo actualizar"});
    		}
      });
    } else {
      res.json({mensaje: "No hay coherencia en los identificadores"});
    }
  })
  .delete(function(req, res) {
    var idUsuario = req.params.idUsuario;
    usuarioModel.delete(idUsuario, function(resultados){
      if(typeof resultados !== undefined) {
        res.json(resultados);
      } else {
        res.json({"Mensaje": "No se elimino el usuario"});
      }
    });
  });

module.exports = usuarioRoute;
