CREATE DATABASE Practica2017;
USE Practica2017;

/*
LAS INSTRUCCIONES ESTAN EN TODO EL DOCUMENTO DDL.
1. NO SE DEBE DE ALTERAR LA ESTRUCTURA DE LA BASE DE DATOS DADA.
2. CREAR EL ARCHIVO CORRESPONDIENTE DE DML PARA LA BASE DE DATOS.
3. EN EL ARCHIVO DML UTILIZAR TODOS LOS PROCEDIMIENTOS DECLARADOS EN EL ARCHIVO DDL.
4. SOLO DE DEBE DE CREAR LOS PROCEDIMIENTOS DE ALMACENADO INDICADOS.
*/


CREATE TABLE Tarea(
	idTarea INT NOT NULL AUTO_INCREMENT,
  titulo VARCHAR(30) NOT NULL,
  descripcion VARCHAR(40) NOT NULL,
	fecha_registro DATETIME NOT NULL,
	fecha_final DATETIME NOT NULL,
  PRIMARY KEY (idTarea)
);

CREATE TABLE Usuario(
	idUsuario INT NOT NULL AUTO_INCREMENT,
  nick VARCHAR(30) NOT NULL,
  contrasena VARCHAR(30) NOT NULL,
  cambios_contrasena INT NOT NULL,
	fecha_registro DATETIME NOT NULL,
  fecha_modificacion DATETIME NULL,
	picture TEXT NULL,
  PRIMARY KEY (idUsuario)
);

CREATE TABLE UsuarioTareas(
	idUsuarioTareas INT NOT NULL AUTO_INCREMENT,
  idUsuario INT NOT NULL,
  idTarea INT NOT NULL,
  PRIMARY KEY (idUsuarioTareas),
  FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario),
  FOREIGN KEY (idTarea) REFERENCES Tarea(idTarea)
);

DELIMITER $$
CREATE PROCEDURE sp_insertUsuario(
_nick VARCHAR(30),
_contrasena VARCHAR(30))
BEGIN
	INSERT INTO Usuario(nick, contrasena, cambios_contrasena, fecha_registro, fecha_modificacion, picture)
    VALUES(_nick, _contrasena, 0, NOW(), NOW(), NULL);
END $$
DELIMITER ;

/*
SP USUARIO UPDATE
CREAR UN PROCEDIMIENTO DE ALMACENADO QUE REALICE LO SIGUIENTE:
1. Modificar el usuario que espere los siguientes parametros:
a. nick
b. contrasena
c. picture
d. idUsuario
2. si se modifica la contraseña llevar el control de los cambios de contraseña
3. actualizar la fecha de modificacion
*/
DROP procedure IF EXISTS sp_usuarioUpdate;
DELIMITER $$
CREATE PROCEDURE sp_usuarioUpdate(
	IN _nick VARCHAR(30),
  IN _contrasena VARCHAR(30),
  In _picture TEXT,
  IN _idUsuario INT)
BEGIN
	SET @contrasena = (SELECT contrasena FROM Usuario WHERE idUsuario = _idUsuario);

	IF @contrasena != _contrasena THEN
		UPDATE Usuario SET nick = _nick, contrasena = _contrasena,
			cambios_contrasena = cambios_contrasena + 1,
			picture = _picture, fecha_modificacion = NOW() WHERE idUsuario = _idUsuario;
	ELSE
		UPDATE Usuario SET nick = _nick, contrasena = _contrasena,
			picture = _picture, fecha_modificacion = NOW() WHERE idUsuario = _idUsuario;
	END IF;
END$$
DELIMITER ;

/*
SP_AUTENTICAR
*/
DELIMITER $$
CREATE PROCEDURE sp_autenticarUsuario(
	_nick VARCHAR(30),
	_contrasena VARCHAR(30))
BEGIN
	SELECT * FROM Usuario WHERE nick = _nick AND contrasena = _contrasena LIMIT 1;
END $$
DELIMITER ;

/*
SP USUARIO SELECT
CREAR UN PROCEDIMIENTO DE ALMACENADO QUE REALICE LO SIGUIENTE:
1. mostrar todos los usuarios con sus datos
2. la consulta debe de llevar lo siguiente:
a. idUsuario,
b. nick
c. contrasena
d. cambios_contrasena
e. fecha_registro
f. hora_registro
g. fecha_modificacion
h. hora_modificacion
i. picture
3. Debe de estar ordenada de los usuarios que han sido modificados ultimamente.
*/
DROP procedure IF EXISTS sp_usuarioSelect;
DELIMITER $$
CREATE PROCEDURE sp_usuarioSelect()
BEGIN
	SELECT idUsuario, nick, contrasena, cambios_contrasena,
		DATE_FORMAT(fecha_registro, '%d/%m/%Y') AS 'fecha_registro',
        DATE_FORMAT(fecha_registro, '%H:%i') AS 'hora_registro',
		DATE_FORMAT(fecha_modificacion, '%d/%m/%Y') AS 'fecha_modificacion',
        DATE_FORMAT(fecha_modificacion, '%H:%i') AS 'hora_modificacion',
        picture
	FROM usuario ORDER BY fecha_modificacion, hora_modificacion DESC;
END$$
DELIMITER ;

/*
SP USUARIO DELETE
CREAR UN PROCEDIMIENTO DE ALMACENADO QUE REALICE LO SIGUIENTE:
1. Eliminar el usuario
2. Eliminar todas las tareas que tenga asignada
*/
DROP procedure IF EXISTS sp_usuarioDelete;
DELIMITER $$
CREATE PROCEDURE sp_usuarioDelete(
	IN id INT)
BEGIN
	DELETE UsuarioTareas, Tarea
	FROM UsuarioTareas
	INNER JOIN Tarea ON Tarea.idTarea = UsuarioTareas.idTarea
	WHERE UsuarioTareas.idUsuario = id;

	DELETE FROM Usuario WHERE idUsuario = id;
END$$
DELIMITER ;

/*
SP TAREA INSERT
CREAR UN PROCEDIMIENTO DE ALMACENADO QUE REALICE LO SIGUIENTE:
1. Registrar una nueva tarea junto al usuario quien lo creo
*/
DROP procedure IF EXISTS sp_tareaInsert;
DELIMITER $$
CREATE PROCEDURE sp_tareaInsert (
	IN _idUsuario INT,
	IN _titulo VARCHAR(30),
	IN _descripcion VARCHAR(40),
	IN _fecha_final DATETIME)
BEGIN
	INSERT INTO Tarea(titulo, descripcion, fecha_registro, fecha_final)
		VALUES (_titulo, _descripcion, NOW(), _fecha_final);

    SET @idTarea = (SELECT idTarea FROM Tarea WHERE titulo = _titulo AND
		descripcion = _descripcion AND fecha_registro = NOW());

	INSERT INTO UsuarioTareas(idUsuario, idTarea)
		VALUES (_idUsuario, @idTarea);
END$$
DELIMITER ;

/*
SP TAREA SELECT
CREAR UN PROCEDIMIENTO DE ALMACENADO QUE REALICE LO SIGUIENTE:
1. Mostrar todas las tareas del usuario por medio de su id
2. Mostrar en la consulta las siguientes columnas.
a. idTarea
b. titulo
c. descripcion
d. fecha_registro
e. hora del registro,
f. fecha_final,
g. hora final
3. la consulta debe de estar ordenada, por fecha y hora.
*/
DROP procedure IF EXISTS sp_tareaSelect;
DELIMITER $$
CREATE PROCEDURE sp_tareaSelect (
	IN _idUsuario INT)
BEGIN
	SELECT ut.idTarea, t.titulo, t.descripcion,
		DATE_FORMAT(t.fecha_registro, '%d/%m/%Y') AS 'fecha_registro',
        DATE_FORMAT(t.fecha_registro, '%H:%i') AS 'hora_registro',
		DATE_FORMAT(t.fecha_final, '%d/%m/%Y') AS 'fecha_final',
        DATE_FORMAT(t.fecha_final, '%H:%i') AS 'hora_final'
	FROM UsuarioTareas ut
		INNER JOIN Tarea t ON ut.idTarea = t.idTarea
    WHERE idUsuario = _idUsuario
    ORDER BY fecha_final, hora_final DESC;
END$$
DELIMITER ;
