var database = require('../config/database.config');
var tarea = {}

tarea.selectAll = function(callback) {
  if(database) {
    var consulta = 'SELECT * from Tarea';
    database.query(consulta,
    function(error, resultados) {
      if(error) throw error;
      if(resultados.length > 0) {
        callback(resultados[0]);
      } else {
        callback(0);
      }
    })
  }
}

tarea.find = function(idTarea, callback) {
  if(database) {
    var consulta = 'SELECT * FROM Tarea WHERE idTarea = ?';
    database.query(consulta, idTarea, function(error, resultados) {
      if(error) throw error;
      if(resultados.length > 0) {
        callback(resultados[0]);
      } else {
        callback(0);
      }
    })
  }
}

tarea.insert = function(data, callback) {
  if(database) {
    var consulta = 'CALL sp_tareaInsert(?, ?, ?, ?)'
    database.query(consulta, [data.idUsuario, data.titulo, data.descripcion, data.fechaFinal],
    function(error, resultado) {
      if(error) throw error;
      callback(resultado);
    });
  }
}

tarea.update = function(data, callback) {
  if(database) {
    var consulta = "UPDATE Tarea SET titulo = ?, descripcion = ?, fecha_final = ? WHERE idTarea = ?";
    database.query(consulta, [data.titulo, data.descripcion, data.fechaFinal, data.idTarea],
    function(error, resultado) {
      if(error) throw error;
      callback(resultado);
    });
  }
}

tarea.delete = function(idTarea, callback) {
  if(database) {
    var consulta = "DELETE Tarea FROM Tarea WHERE idTarea = ? ";
    database.query(consulta, idTarea, function(error, resultado) {
      if(error) throw error;
      callback({"Mensaje": "Eliminado"});
    });
  }
}

module.exports = tarea;
