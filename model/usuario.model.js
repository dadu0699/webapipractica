var database = require('../config/database.config');
var usuario = {};

usuario.selectAll = function(callback) {
  if(database) {
		var consulta = 'CALL sp_usuarioSelect()';
    database.query(consulta,
    function(error, resultados) {
      if(error) throw error
      if(resultados.length > 0) {
        callback(resultados[0]);
      } else {
        callback(0);
      }
    })
  }
}

usuario.find = function(idUsuario, callback) {
  if(database) {
  	var consulta = 'SELECT * FROM Usuario WHERE idUsuario = ?';
    database.query(consulta, idUsuario, function(error, resultados) {
      if(error) throw error;
      if(resultados.length > 0) {
        callback(resultados[0]);
      } else {
        callback(0);
      }
    })
  }
}

usuario.insert = function(data, callback) {
  if(database) {
    database.query("CALL sp_insertUsuario(?,?)", [data.nick, data.contrasena],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback({"insertId": resultado.insertId});
      }
    });
  }
}

usuario.update = function(data, callback) {
  if(database) {
    var sql = "CALL sp_usuarioUpdate(?, ?, ?, ?)";
    database.query(sql,
    [data.nick, data.contrasena, data.picture, data.idUsuario],
    function(error, resultado) {
      if(error) throw error;
      callback(data);
    });
  }
}

usuario.delete = function(idUsuario, callback) {
  if(database) {
    var sql = "CALL sp_usuarioDelete(?)";
    database.query(sql, idUsuario,
    function(error, resultado) {
      if(error) throw error;
      callback({"Mensaje": "Eliminado"});
    });
  }
}

module.exports = usuario;
